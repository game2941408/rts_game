using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.AI;
using static UnityEngine.GraphicsBuffer;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    [Header("References")]
    private PlayerMovement playerMovement;
    private Interactable target;
    
    [Header("Game Object")]
    [SerializeField] private GameObject lamp;

    [Header("Components")]
    [SerializeField] private ParticleSystem clickEffect;
    [SerializeField] private LayerMask clickableLayers;
    [SerializeField] private LayerMask hoverLayer;
    private NavMeshAgent agent;
    private AudioSource footstep;
    private Transform _hoverable;

    [Header("Clones")]
    private GameObject clickEffectClone;
    private GameObject lampClone;


    [Header("Animations")]
    private Animator playerAnimation;
    const string IDLE = "Male Idle";
    const string WALK = "Male_Walk";

    [Header("Bools")]
    public bool menuOpen = false;
    private bool playerBusy = false;
    private bool instantiated = false;
    private bool isWalking = false;

    [Header("Variables")]
    private float interactDistance = 2f;
    private float lookRotationSpeed = 5f;


    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        playerAnimation = GetComponent<Animator>();
        footstep = GetComponent<AudioSource>();
        footstep.enabled = false;
        playerMovement = new PlayerMovement();
        AssignInputs();
    }

    private void Update()
    {
        FollowTarget();
        FaceTarget();
        SetAnimation();
        PlaySteps();
        HoverObject();
        DeleteEffects();
    }

    void OnEnable()
    {
        playerMovement.Enable();
    }

    void OnDisable()
    {
        playerMovement.Disable();
    }

    private void AssignInputs()
    {
        playerMovement.Player.Move.performed += ctx => ClickToMove();
    }

    private void ClickToMove()
    {
        RaycastHit hit;
        if (menuOpen)
            return;
        else
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100, clickableLayers))
            {
                if (hit.transform.CompareTag("Interactable"))
                {
                    target = hit.transform.GetComponent<Interactable>();
                    if (clickEffect != null)
                        Instantiate(clickEffect, hit.transform.position + new Vector3(0, 0.1f, 0), clickEffect.transform.rotation);
                }
                else
                {
                    target = null;
                    agent.destination = hit.point;
                    if (clickEffect != null)
                        Instantiate(clickEffect, hit.point + new Vector3(0, 0.1f, 0), clickEffect.transform.rotation);
                }
            }
        }
    }

    private void FaceTarget()
    {
        if (agent.destination == transform.position) return;

        Vector3 facing = Vector3.zero;
        if (target != null)
        { facing = target.transform.position; }
        else
        { facing = agent.destination; }

        Vector3 direction =(facing - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        if (agent.velocity.sqrMagnitude > Mathf.Epsilon)
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * lookRotationSpeed);
    }

    private void ReachDistance()
    {
        agent.SetDestination(transform.position);

        if (playerBusy) return;

        playerBusy = true;

        switch (target.interactionType)
        {
            case InteractableType.Chest:

                target.InteractWithChest();
                target = null;
                playerBusy = false;
                SetAnimation();
                break;

            case InteractableType.Key:

                target.InteractWithKey();
                target = null;
                playerBusy = false;
                SetAnimation();
                break;

            case InteractableType.Door:

                target.InteractWithDoor();
                target = null;
                playerBusy = false;
                SetAnimation();
                break;
        }
    }

    private void FollowTarget()
    {
        if (target == null) 
            return;

        if (Vector3.Distance(target.transform.position, transform.position) <= interactDistance)
            ReachDistance();
        else
            agent.SetDestination(target.transform.position);
    }

    private void SetAnimation()
    {
        if (playerBusy)
            return;

        if (agent.velocity == Vector3.zero)
        {
            playerAnimation.Play(IDLE);
            isWalking = false;
        }

        else
        {
            playerAnimation.Play(WALK);
            isWalking = true;
        }

    }

    private void PlaySteps()
    {
        if (isWalking)
            footstep.enabled = true;
        else
            footstep.enabled = false;

    }

    private void HoverObject()
    {
        RaycastHit hit;
        bool hitSomething = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100, hoverLayer);
        if (menuOpen)
            return;
        else
        {
            if (hitSomething && !_hoverable && !instantiated)
            {
                if (hit.transform.CompareTag("Interactable"))
                {
                    Instantiate(lamp, new Vector3(hit.transform.position.x, 11, hit.transform.position.z), lamp.transform.rotation);
                    _hoverable = hit.transform;
                    instantiated = true;
                }
            }
            else if (!hitSomething)
            {
                instantiated = false;
                _hoverable = null;
            }
        } 
    }

    private void DeleteEffects()
    {
        clickEffectClone = GameObject.Find("Click_prefab(Clone)");
        Destroy(clickEffectClone, 0.4f);
        if (!instantiated)
        {
            lampClone = GameObject.Find("Lamp_prefab(Clone)");
            Destroy(lampClone);
        }
    }  
}

   
