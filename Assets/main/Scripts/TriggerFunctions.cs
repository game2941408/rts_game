using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerFunctions : MonoBehaviour
{
    [Header("References")]
    private ButtonsFunction buttonsFunction;
    private PlayerController playerController;

    [Header("Game Objects")]
    [SerializeField] private GameObject chestMenu;
    [SerializeField] private GameObject keyMenu;
    [SerializeField] private GameObject doorMenu;
    [SerializeField] private GameObject doorMenuWOKey;
    private GameObject player;

    [Header("Components")]
    [SerializeField] private AudioSource menuAudio;

    [Header("Variables")]
    private float delay = 2f;
    private bool canInteract = true;

    private void Start()
    {
        Interactable.ChestEvent += ChestTrigger;
        Interactable.KeyEvent += KeyTrigger;
        Interactable.DoorEvent += DoorTrigger;
    }

    private void OnDisable()
    {
        Interactable.ChestEvent -= ChestTrigger;
        Interactable.KeyEvent -= KeyTrigger;
        Interactable.DoorEvent -= DoorTrigger;
    }


    public void ChestTrigger()
    {
        if (canInteract)
        {
            chestMenu.SetActive(true);
            player = GameObject.Find("Player_prefab(Clone)");
            playerController = player.GetComponent<PlayerController>();
            playerController.menuOpen = true;
            menuAudio.Play();
            canInteract = false;
            Invoke("ResetInteraction", delay);
        }
    }

    public void KeyTrigger()
    {
        if (canInteract)
        {
            keyMenu.SetActive(true);
            player = GameObject.Find("Player_prefab(Clone)");
            playerController = player.GetComponent<PlayerController>();
            playerController.menuOpen = true;
            menuAudio.Play();
            canInteract = false;
            Invoke("ResetInteraction", delay);
        }
    }

    public void DoorTrigger()
    {
        if (canInteract)
        {
            buttonsFunction = GetComponent<ButtonsFunction>();
            player = GameObject.Find("Player_prefab(Clone)");
            playerController = player.GetComponent<PlayerController>();
            playerController.menuOpen = true;
            if (buttonsFunction.haveKey)
            {
                doorMenu.SetActive(true);
                menuAudio.Play();
            }
            else
            {
                doorMenuWOKey.SetActive(true);
                menuAudio.Play();
            }
            canInteract = false;
            Invoke("ResetInteraction", delay);
        }
    }

    private void ResetInteraction()
    {
        canInteract = true;
    }
}
