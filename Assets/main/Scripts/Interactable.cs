using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System;
public enum InteractableType { Door, Key, Chest }

public class Interactable : MonoBehaviour
{
    [Header("References")]
    public InteractableType interactionType;

    public static event Action ChestEvent;
    public static event Action KeyEvent;
    public static event Action DoorEvent;

    public void InteractWithDoor()
    {
        DoorEvent?.Invoke();
    }

    public void InteractWithChest()
    {
        ChestEvent?.Invoke();
    }

    public void InteractWithKey()
    {
        KeyEvent?.Invoke();
    }
}
