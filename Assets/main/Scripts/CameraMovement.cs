using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class CameraMovement : MonoBehaviour
{
    [Header("References")]
    private CameraControlActions cameraActions;
    private PlayerController playerController;
    private InputAction movement;

    [Header("Game Objects")]
    private GameObject player;

    [Header("Components")]
    private Transform cameraTransform;

    [Header("Variables")]
    [SerializeField] private float maxSpeed = 5f; 
    [SerializeField] private float acceleration = 10f;
    [SerializeField] private float damping = 15f;
    [SerializeField] private float maxRotationSpeed = 1f;
    private float speed;

    [Header("Vector3")]
    private Vector3 targetPosition;
    private Vector3 horizontalVelocity;
    private Vector3 lastPosition;

    private void Awake()
    {
        cameraActions = new CameraControlActions();
        cameraTransform = this.GetComponentInChildren<Camera>().transform;
    }

    private void OnEnable()
    {
        lastPosition = this.transform.position;
        movement = cameraActions.Camera.Movement;
        
        cameraActions.Camera.Enable();
    }

    private void OnDisable()
    {
        
        cameraActions.Disable();
    }

    private void Update()
    {
        GetKeyboardMovement();
        UpdateVelocity();
        UpdateBasePosition();
        CheckBoundries();
    }

    private void FixedUpdate()
    {
        RotateCamera();
    }

    private void UpdateVelocity()
    {
        horizontalVelocity = (this.transform.position - lastPosition) / Time.deltaTime;
        horizontalVelocity.y = 0;
        lastPosition = this.transform.position;
    }

    private void GetKeyboardMovement()
    {
        Vector3 inputValue = movement.ReadValue<Vector2>().x * GetCameraRight()
                    + movement.ReadValue<Vector2>().y * GetCameraForward();

        inputValue = inputValue.normalized;
        player = GameObject.Find("Player_prefab(Clone)");
        playerController = player.GetComponent<PlayerController>();
        if (playerController.menuOpen)
            return;
        else
        {
            if (inputValue.sqrMagnitude > 0.1f)
                targetPosition += inputValue;
        }
    }

    private Vector3 GetCameraRight()
    {
        Vector3 right = cameraTransform.right;
        right.y = 0;
        return right;
    }

    private Vector3 GetCameraForward()
    {
        Vector3 forward = cameraTransform.forward;
        forward.y = 0;
        return forward;
    }

    private void UpdateBasePosition()
    {
        if (targetPosition.sqrMagnitude > 0.1f)
        {
            speed = Mathf.Lerp(speed, maxSpeed, Time.deltaTime * acceleration);
            transform.position += targetPosition * speed * Time.deltaTime;
        }
        else
        {
            horizontalVelocity = Vector3.Lerp(horizontalVelocity, Vector3.zero, Time.deltaTime * damping);
            transform.position += horizontalVelocity * Time.deltaTime;
        }

        targetPosition = Vector3.zero;
    }

    private void RotateCamera()
    {
        player = GameObject.Find("Player_prefab(Clone)");
        playerController = player.GetComponent<PlayerController>();
        float inputLeft = cameraActions.Camera.RotateLeft.ReadValue<float>();
        float inputRight = cameraActions.Camera.RotateRight.ReadValue<float>();
        if (playerController.menuOpen)
            return;
        else
        {
            if (inputLeft == 1)
            {
                transform.rotation = Quaternion.Euler(0f, inputLeft * maxRotationSpeed + transform.rotation.eulerAngles.y, 0f);
            }
            if (inputRight == 1)
            {
                transform.rotation = Quaternion.Euler(0f, -inputRight * maxRotationSpeed + transform.rotation.eulerAngles.y, 0f);
            }
        }
    }

    private void CheckBoundries()
    {
        if(transform.position.x >= 5f)
            transform.position = new Vector3(5f, 0, transform.position.z);

        if (transform.position.x <= -5f)
            transform.position = new Vector3(-5f, 0, transform.position.z);

        if (transform.position.z >= 5f)
            transform.position = new Vector3(transform.position.x, 0, 5f);

        if (transform.position.z <= -5f)
            transform.position = new Vector3(transform.position.x, 0, -5f);
    }
}
