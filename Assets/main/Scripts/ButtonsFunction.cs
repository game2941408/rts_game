using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.AI.Navigation;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class ButtonsFunction : MonoBehaviour
{
    [Header("References")]
    private PlayerController playerController;

    [Header("Game Objects")]
    [SerializeField] private GameObject chest;
    [SerializeField] private GameObject key;
    [SerializeField] private GameObject door;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject bookcase;
    [SerializeField] private GameObject cameraHolder;
    [SerializeField] private GameObject openChest;

    [Header("Components")]
    [SerializeField] private AudioSource buttonClick;
    [SerializeField] private Animator chestAnimation = null;
    [SerializeField] private NavMeshSurface surface;
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private TextMeshProUGUI bestTimeText;
    [SerializeField] private TextMeshProUGUI inGameText;

    [Header("Clones")]
    private GameObject chestClone;
    private GameObject keyClone;
    private GameObject doorClone;
    private GameObject playerClone;
    private GameObject bookcase1Clone;
    private GameObject bookcase2Clone;
    private GameObject bookcase3Clone;
    private GameObject bookcase4Clone;
    private GameObject cameraHolderClone;
    private GameObject openChestClone;

    [Header("Menus")]
    [SerializeField] private GameObject chestMenu;
    [SerializeField] private GameObject doorMenu;
    [SerializeField] private GameObject keyMenu;
    [SerializeField] private GameObject startMenu;
    [SerializeField] private GameObject doorMenuWOKey;
    [SerializeField] private GameObject gameOvertMenu;

    [Header("Bools")]
    public bool haveKey = false;
    private bool opened = false;
    private bool timerActive = false;

    [Header("Variables")]
    private float chestMaxX = 8f;
    private float chestMinX = -8f;
    private float chestMaxZ = 8f;
    private float chestMinZ = -8f;
    private float chestRandomX = 0f;
    private float chestRandomZ = 0f;
    private float chestRotation = 0f;
    private float delay = 0.9f;
    private float elapsedTime = 0f;
    private float bestTime;


    
    private void Start()
    {
        if (PlayerPrefs.HasKey("Best"))
            bestTime = PlayerPrefs.GetFloat("Best");
        else
            bestTime = 99999f;
    }

    private void Update()
    {
        WorkingTimer();
        if(opened)
            StartCoroutine(SpawnOpenChest());
    }

    public void GameStart()
    {
        startMenu.SetActive(false);
        cameraHolderClone = Instantiate(cameraHolder, Vector3.zero, Quaternion.identity);
        playerClone = Instantiate(player, transform.position, Quaternion.identity);
        PlaceChest();
        PlaceDoor();
        StartTimer();
        surface.BuildNavMesh();
        buttonClick.Play();
    }

    public void ExitGame()
    {
        Application.Quit();
    }


    public void ChestYes()
    {
        chestMenu.SetActive(false);
        opened = true;
        chestAnimation = chestClone.GetComponent<Animator>();
        chestAnimation.Play("ChestOpen", 0, 0.0f);
        Destroy(chestClone, 1f);
        keyClone = Instantiate(key, new Vector3(chestRandomX, 0f, chestRandomZ), Quaternion.Euler(new Vector3(0, chestRotation, 0)));
        playerController = playerClone.GetComponent<PlayerController>();
        playerController.menuOpen = false;
        buttonClick.Play();
    }

    public void ChestNo()
    {
        chestMenu.SetActive(false);
        playerController = playerClone.GetComponent<PlayerController>();
        playerController.menuOpen = false;
        buttonClick.Play();
    }

    public void KeyYes()
    {
        keyMenu.SetActive(false);
        Destroy(keyClone);
        haveKey = true;
        playerController = playerClone.GetComponent<PlayerController>();
        playerController.menuOpen = false;
        buttonClick.Play();
    }

    public void KeyNo()
    {
        keyMenu.SetActive(false);
        playerController = playerClone.GetComponent<PlayerController>();
        playerController.menuOpen = false;
        buttonClick.Play();
    }

    public void DoorYes()
    {
        doorMenu.SetActive(false);
        Destroy(doorClone);
        Destroy(bookcase1Clone);
        Destroy(bookcase2Clone);
        Destroy(bookcase3Clone);
        Destroy(bookcase4Clone);
        Destroy(cameraHolderClone);
        Destroy(openChestClone);
        chestRandomX = 0f;
        chestRandomZ = 0f;
        chestRotation = 0f;
        haveKey = false;
        opened = false;
        Destroy(playerClone);
        gameOvertMenu.SetActive(true);
        StopTimer();
        SetBestTime();
        buttonClick.Play();
    }

    public void DoorNo()
    {
        doorMenu.SetActive(false);
        playerController = playerClone.GetComponent<PlayerController>();
        playerController.menuOpen = false;
        buttonClick.Play();
    }

    public void DoorOk()
    {
        doorMenuWOKey.SetActive(false);
        playerController = playerClone.GetComponent<PlayerController>();
        playerController.menuOpen = false;
        buttonClick.Play();
    }

    public void TryAgain()
    {
        gameOvertMenu.SetActive(false);
        cameraHolderClone = Instantiate(cameraHolder, Vector3.zero, Quaternion.identity);
        playerClone = Instantiate(player, transform.position, Quaternion.identity);
        PlaceChest();
        PlaceDoor();
        surface.BuildNavMesh();
        elapsedTime = 0f;
        StartTimer();
        buttonClick.Play();
    }

    private void PlaceChest()
    {
        while (chestRandomX < 1 && chestRandomX > -1 || chestRandomZ < 1 && chestRandomZ > -1)
        {
            chestRandomX = Random.Range(chestMinX, chestMaxX);
            chestRandomZ = Random.Range(chestMinZ, chestMaxZ);
            chestRotation = Random.Range(0f, 360f);
        }

        chestClone = Instantiate(chest, new Vector3(chestRandomX, 0f, chestRandomZ), Quaternion.Euler(new Vector3(0, chestRotation, 0)));

    }

    private void PlaceDoor()
    {
        int number = Random.Range(1, 5);
        if (number == 1)
        {
            doorClone = Instantiate(door, new Vector3(10f, 0f, 0f), Quaternion.Euler(new Vector3(0, -90f, 0)));
            bookcase2Clone = Instantiate(bookcase, new Vector3(-9.335f, 0f, 0f), Quaternion.Euler(new Vector3(0, -90f, 0)));
            bookcase3Clone = Instantiate(bookcase, new Vector3(0f, 0f, 9.335f), Quaternion.identity);
            bookcase4Clone = Instantiate(bookcase, new Vector3(0f, 0f, -9.335f), Quaternion.Euler(new Vector3(0, -180f, 0)));
        }
        if (number == 2)
        {
            doorClone = Instantiate(door, new Vector3(-10f, 0f, 0f), Quaternion.Euler(new Vector3(0, 90f, 0)));
            bookcase1Clone = Instantiate(bookcase, new Vector3(9.335f, 0f, 0f), Quaternion.Euler(new Vector3(0, 90f, 0)));
            bookcase3Clone = Instantiate(bookcase, new Vector3(0f, 0f, 9.335f), Quaternion.identity);
            bookcase4Clone = Instantiate(bookcase, new Vector3(0f, 0f, -9.335f), Quaternion.Euler(new Vector3(0, -180f, 0)));
        }
        if (number == 3)
        {
            doorClone = Instantiate(door, new Vector3(0f, 0f, 10f), Quaternion.Euler(new Vector3(0, 180f, 0)));
            bookcase1Clone = Instantiate(bookcase, new Vector3(9.335f, 0f, 0f), Quaternion.Euler(new Vector3(0, 90f, 0)));
            bookcase2Clone = Instantiate(bookcase, new Vector3(-9.335f, 0f, 0f), Quaternion.Euler(new Vector3(0, -90f, 0)));
            bookcase4Clone = Instantiate(bookcase, new Vector3(0f, 0f, -9.335f), Quaternion.Euler(new Vector3(0, -180f, 0)));
        }
        if (number == 4)
        {
            doorClone = Instantiate(door, new Vector3(0f, 0f, -10f), Quaternion.identity);
            bookcase1Clone = Instantiate(bookcase, new Vector3(9.335f, 0f, 0f), Quaternion.Euler(new Vector3(0, 90f, 0)));
            bookcase2Clone = Instantiate(bookcase, new Vector3(-9.335f, 0f, 0f), Quaternion.Euler(new Vector3(0, -90f, 0)));
            bookcase3Clone = Instantiate(bookcase, new Vector3(0f, 0f, 9.335f), Quaternion.identity);
        }
    }

    private IEnumerator SpawnOpenChest()
    {
        yield return new WaitForSeconds(delay);
        if (opened)
            openChestClone = Instantiate(openChest, new Vector3(chestRandomX, 0f, chestRandomZ), Quaternion.Euler(new Vector3(0, chestRotation, 0)));
        opened = false;
    }

    private void StartTimer()
    {
        timerActive = true;
    }

    private void StopTimer()
    {
        timerActive = false;
    }


    private void SetBestTime()
    {
        if(bestTime > elapsedTime)
        {
            PlayerPrefs.SetFloat("Best", elapsedTime);
            bestTime = PlayerPrefs.GetFloat("Best");
            float minutes = Mathf.FloorToInt(bestTime / 60);
            float seconds = Mathf.FloorToInt(bestTime % 60);
            float milliseconds = bestTime % 1 * 1000;
            bestTimeText.text = string.Format("Best time: {0:00}:{1:00}:{2:000}", minutes, seconds, milliseconds);
        }
        else
        {
            float minutes = Mathf.FloorToInt(bestTime / 60);
            float seconds = Mathf.FloorToInt(bestTime % 60);
            float milliseconds = bestTime % 1 * 1000;
            bestTimeText.text = string.Format("Best time: {0:00}:{1:00}:{2:000}", minutes, seconds, milliseconds);
        }
    }

    private void WorkingTimer()
    {
        if (timerActive)
        {
            elapsedTime += Time.deltaTime;
            float minutes = Mathf.FloorToInt(elapsedTime / 60);
            float seconds = Mathf.FloorToInt(elapsedTime % 60);
            float milliseconds = elapsedTime % 1 * 1000;
            inGameText.text = string.Format("Time: {0:00}:{1:00}:{2:000}", minutes, seconds, milliseconds);
            timerText.text = string.Format("Time: {0:00}:{1:00}:{2:000}", minutes, seconds, milliseconds);
        }
    }
}
